exports.config = {
  /** iconfont 仓库id */
  pid: 2586413,
  /** 输出文件夹 确保文件夹存在 */
  outputPath: './iconfont',
  /** 添加前缀（用于iconfont字体重复问题）例如：my */
  prefix: '',
  /** cookie */
  cookie: '',
}