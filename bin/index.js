#!/usr/bin/env node

const ora = require('ora');
const qs = require('qs')
const request = require('axios');
const program = require('commander');
const fs = require('fs');
const path = require('path');
const cProcess = require('child_process');
const extract = require('extract-zip');

const {templatePath, configFileName, configFilePath} = require('../constants/index')

const processExecSync = shell => new Promise((resolve, reject) => {
  cProcess.exec(shell, (error, stdout) => {
    if(error !== null) {
      reject(error);
    } else {
      resolve(stdout);
    }
  });
});

// const getToken = () => {
//   return /ctoken=(\S*);/.exec(cookie)[1]
// }

/* 生成配置文件 */
const initConfig = () => {
  const spinner = ora(`${configFileName} 生成配置文件...`).start();
  try {

    if(fs.existsSync(configFilePath)) {
      throw `${configFileName} 文件已存在，生成失败！`
    }

    /* 模板内容 */
    const fileContent = fs.readFileSync(path.resolve(templatePath, configFileName));

    /* 写入内容 */
    fs.writeFileSync(configFilePath, fileContent);

    spinner.succeed();
  } catch(e) {
    spinner.fail(e);
  }
}

const updateIconfont = async () => {
  const spinner = ora('更新并下载iconfont...').start();

  const {config: {prefix, cookie, pid, outputPath}} = require(configFilePath);

  /** 第一步下载iconfont.zip */
  const response = await request.get('https://www.iconfont.cn/api/project/download.zip?pid=' + pid, {headers: {cookie}, responseType: 'stream'});
  const zipFilePath = path.resolve(outputPath, 'iconfont.zip')
  response.data.pipe(fs.createWriteStream(zipFilePath));
  response.data.on('end', async () => {
    /** 解压 zip */
    const tempPath = path.resolve(outputPath, 'temp')
    await extract(zipFilePath, {dir: tempPath})
    const result = fs.readdirSync(tempPath);
    const dirPath = path.resolve(tempPath, result[0]);
    /** 替换文件  */
    await processExecSync(`
      cd ${dirPath} 
      mv iconfont.css ../../${prefix}iconfont.css
      mv iconfont.js ../../${prefix}iconfont.js
      mv iconfont.ttf ../../${prefix}iconfont.ttf
      mv iconfont.woff ../../${prefix}iconfont.woff
      mv iconfont.woff2 ../../${prefix}iconfont.woff2
    `)
    /** 删除文件夹 */
    await processExecSync('rm -rf ' + tempPath);
    await processExecSync('rm -rf ' + zipFilePath);

    /** 统一添加前缀前缀 */
    await processExecSync(`
      cd ${outputPath}
      sed -i.bak "s/iconfont/${prefix}iconfont/g" ${prefix}iconfont.css
      sed -i.bak "s/.icon-/.${prefix}icon-/g" ${prefix}iconfont.css
      sed -i.bak "s/icon-/${prefix}icon-/g" ${prefix}iconfont.js
    `);
  });


  spinner.succeed();
};

program
  .command('init')
  .description('自动生成配置文件')
  .action(initConfig);

program
  .command('update')
  .description('自动本地化iconfont文件')
  .action(updateIconfont);

program
  .parse(process.argv);
