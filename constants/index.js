const path = require('path');

/** 指令输入路径 */
const cmdPath = process.cwd();
exports.cmdPath = cmdPath;

/** 模板文件夹路径 */
exports.templatePath = path.resolve(__dirname, '../template');

/** 配置文件名称和路径 */
const configFileName = 'iconfontConfig.js';
exports.configFileName = configFileName;
exports.configFilePath = path.resolve(cmdPath, configFileName);