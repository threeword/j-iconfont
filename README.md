# jiconfont

#### 介绍

自动下载iconfont字体文件到本地、自动添加给字体引入名称和使用名称添加前缀
#### 前言

#### 安装教程

```bash
# 本地安装
npm i @threeword/jiconfont -D

```

#### 使用说明

### 第一步：初始化配置文件

```bash
npx jiconfont init
```

会在根目录下生成 iconfontConfig.js
``` js
/** iconfontConfig.js */

exports.config = {
  /** iconfont 仓库id */
  pid: 001,
  /** 输出文件夹 确保文件夹存在 */
  outputPath: './iconfont',
  /** 添加前缀（用于iconfont字体重复问题）例如：my- */
  prefix: 'my-',
  /** cookie */
  cookie: '',
}
```

###### 配置说明
- pid : iconfont 仓库id ![输入图片说明](./image/pid.png)
- outputPath : 输出文件夹 确保文件夹存在
- prefix : 添加前缀（用于iconfont字体重复问题）例如：my- */
- cookie : cookie![输入图片说明](./image/cookie.png)

### 第二步：下载iconfont文件

```bash
npx jiconfont update
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
